Production Build Status: [ ![Codeship Status for dafightingmongoosi/Cobra-Chat](https://www.codeship.io/projects/b9291d40-446e-0132-43bc-1e738e05cfd5/status)](https://www.codeship.io/projects/44833)
Development Build Status: [ ![Codeship Status for dafightingmongoosi/Cobra-Chat](https://www.codeship.io/projects/b9291d40-446e-0132-43bc-1e738e05cfd5/status?branch=DVLP)](https://www.codeship.io/projects/44833)

# cobra-chat



## Usage



## Developing
Requires:

  * MongoDb running on port 27017
  * Environment variables PORT=(default is 3000) and NODE_ENV=development

Branch from the DVLP branch for the most current, accepted development code. Rename your branch locally to the feature you're developing, push the new branch to the repo and create a pull request.

### config.json
Cobra chat expects for there to be a config.json file located in the config directory (src/config during development)
The config.json file requires only one entry, sessionSecret, which is used for express session management in app.js

### auth.js
Cobra chat expects for there to be an auth.js file located in the config directory (src/config during development)
The auth.js file should export settings for use with passport.js. Example:

```json
'googleAuth' : {
  'clientID' : 'the_client_id_from_google_developer_console',
  'clientSecret' : 'the_client_secret_key',
  'callbackURL' : 'the_url_that_google_will_send_auth_responses_to' // for local development use http://localhost:<port>/auth/google/callback (there must be an associated entry in google console)
 }
```
## Tools

### Gulp
For development, Cobra-Chat uses gulp.js. For a fresh install run ``` npm install && gulp ``` OR run ```npm install && gulp clean && gulp copy && gulp app && gulp less && gulp server:start ``` (either way from within the cobra-chat directory)
This will cause the source code to be distributed within the project in the manner that app.js expects and will start the server.

For server deployments, run ``` gulp deploy ``` then copy the contents of the resulting "dist/" folder to your deployment directory (ex: /opt/cobra-chat/ ) then within that directory run ``` npm install --production ``` to install only the runtime dependencies. Then run ``` node app.js ``` to start the server
