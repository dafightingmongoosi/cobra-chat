(function() {
    'use strict';
    var gulp = require('gulp'),
        gutil = require('gulp-util'),
        less = require('gulp-less'),
        rename = require('gulp-rename'),
        minifycss = require('gulp-minify-css'),
        server = require('gulp-develop-server'),
        livereload = require('gulp-livereload'),
        del = require('del'),
        mocha = require('gulp-mocha');

    var srcWatch = [    './src/config/*.js',
                        './src/models/*.js',
                        './src/routes/*.js',
                        './src/views/*.ejs'
                     ],
        paths = {
            config: ['config/*.*','!config/.gitignore'],
            models: ['models/*.js'],
            routes: ['routes/*.js'],
            views: ['views/*.ejs']
        },
        bases = {
            app: '.',
            src: './src'
        };

    gulp.task('less', function(cb) {
        var sourceLESS = 'src/less/',
            targetCSS = 'public/vendor/css';

        gulp.src(sourceLESS + '/twbs.less')
            .pipe(less().on('error', gutil.log))
            .pipe(minifycss())
            .pipe(rename('bootstrap.min.css'))
            .pipe(gulp.dest(targetCSS));
        cb();
    });

    gulp.task('clean', function(cb){
        var cleanList = [];
        for (var path in paths){
            cleanList.push(bases.app.concat("/" + paths[path][0]));
        }
        del.sync(cleanList); // block so dependent tasks will wait for completion
        cb();
    });

    gulp.task('copy', ['clean'], function(cb) {
        // copy the src folders based on the paths variable
        for ( var path in paths ) {
            gulp.src(bases.src.concat("/" + paths[path][0]), {cwd: bases.app})
                .pipe(gulp.dest(path));
        }
        cb();
    });

    gulp.task('app', function(cb){
        // copy the app.js file
        gulp.src(bases.src.concat("/" + "app.js"), {cwd: bases.app})
            .pipe(gulp.dest(bases.app));
        cb();
    });

    gulp.task('server:start', function() {
        var options = { "path" : './app.js'};
        setTimeout(function(){ // use the timeout to give the OS time to finish copying files
            server.listen(options, livereload.listen );
        },3000);
    });

    gulp.task('default', ['copy','app','less','server:start'], function() {
        var lessDir = ['./src/*.less', './src/**/*.less'];

        function restart( err, file ) {
           server.changed( function( error ) {
               if( ! err ){
                   livereload.changed(file.path);
               }
           });
        };
        gulp.watch( srcWatch, ['copy'], restart);
        gulp.watch( lessDir, ['less'] );
        // doesnt work gulp.watch( './app.js', ['server.kill','app','server:start']);
    });

    gulp.task('fresh', ['clean', 'copy', 'app']);

    gulp.task('deploy', ['less'], function(){
        // for deploying on my linux server, copies src and public to a dist folder
        var dist = "dist/";

        for ( var path in paths ) {
            gulp.src(bases.src.concat("/" + paths[path][0]), {cwd: bases.app})
                .pipe(gulp.dest(dist + path));
        }

        gulp.src(bases.src.concat("/" + "app.js"), {cwd: bases.app})
            .pipe(gulp.dest(dist));
        gulp.src(bases.app + "/public/**/*.*", {cwd: bases.app})
            .pipe(gulp.dest(dist + "public"));
        gulp.src(bases.app.concat("/package.json"), {cwd: bases.app})
            .pipe(gulp.dest(dist));
    });

    gulp.task('run-tests', function(){
        return gulp.src('src/tests/*.*', {read: false})
            .pipe(mocha({   reporter: 'landing',
                            ui: 'bdd',
                            timeout: 2000,
                            bail: false }));
    });
}());