
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var https = require('https');
var socketio = require('socket.io');
var mongoose = require('mongoose');
var passport = require('passport');
var path = require('path');
var routes = require('./routes');
var user = require('./routes/user');
var flash    = require('connect-flash');
var settingsFile = require('./config/config.json');
var fs = require('fs');
var MongoStore = require('connect-mongo')(express);
var app = express();

var configDB = require('./config/database.js');

if (settingsFile.sslKey && settingsFile.sslCert){
    var ssl = {
        key: fs.readFileSync(settingsFile.sslKey),
        cert: fs.readFileSync(settingsFile.sslCert)
    };
}
// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.session({secret: settingsFile.sessionSecret,
    store: new MongoStore({
        mongoose_connection: mongoose.connections[0]
    })},
    function(err){
        if (err) {
            return console.error('Failed connecting mongostore for storing session data. %s', err.stack);
        }
        return console.log('Connected mongostore for storing session data');
    }
));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.errorHandler());


app.get('/', routes.index);
app.get('/users', user.list);
require('./routes/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

if (ssl){
    var server = https.createServer(ssl,app);
}else{
    var server = http.createServer(app);
}
var io = socketio.listen(server);

server.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port') + ' and env=' + process.env.NODE_ENV);
});

io.sockets.on('connection', function (socket) {
    //console.log("CONNECTION!");
    require('./routes/socketChat').listen(io, socket);
});
