var userList = [];
module.exports.listen = function (io, socket) {
    'use strict';
    socket.on('send-message', function (data) {
        var newMessage =  data.message;
        var newDisplayName = data.displayName;
        var newImage = data.image;
        var curTime = new Date();
        var timestamp = (curTime.getMonth() + 1) + '/' +
            curTime.getDate() + ' ' +
            curTime.toLocaleTimeString();
        newMessage = newMessage.replace(/\</g,"&lt;");
        newMessage = newMessage.replace(/\>/g,"&gt;");
        newMessage = newMessage.replace(/\n/g, "<br/>");

        io.sockets.emit('new-message', {
            timestamp: curTime.valueOf(),
            displayName: newDisplayName,
            message: newMessage,
            image: newImage
        });
    });
    /*
     Tracking by socket rather than userName
     */
    socket.on('disconnect', function () {
      //console.log("We disconnected");
        for (var i = 0; i < userList.length; i++) {
          if ( userList[i].id === socket.id ){
            break;
          }
        }
      userList.splice(i,1);
      io.sockets.emit('user-list', {
        userList : userList
      });
    });

    socket.on('userName', function (data){
      var userObj = {};
      userObj.id = socket.id;
      userObj.userName = data.userName;
      userList.push(userObj);
      io.sockets.emit('user-list', {
        userList : userList
      });
    });
};